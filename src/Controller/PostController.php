<?php

namespace App\Controller;

use App\Entity\Post\Post;
use App\Entity\User\User;
use App\Form\PostType;
use App\Manager\PostManager;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class PostController extends Controller
{
    private $postService;

    public function __construct(PostManager $postService)
    {
        $this->postService = $postService;
    }

    /**
     * @Route("/{userId}/create", name="user_post")
     * @param Request $request
     */

    public function createpost(Request $request, $userId)
    {

        $user = $this->getUser();
        if (!$user)
        {
            return $this->redirectToRoute('login_user');
        }
        $post = new Post();
        $form = $this->createForm(PostType::class, $post);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid())
        {

                $this->postService->createPost($post, $user, $userId );


        }

       // $this->postService->createPost($post, $user);
        return $this->render('post/create.html.twig', [
        'form' => $form->createView()
        ]);

    }
}

<?php
namespace App\Repository;


use App\Entity\Post\Post;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr;
use Symfony\Bridge\Doctrine\RegistryInterface;
use Doctrine\ORM\EntityManager;



class PostRepository extends ServiceEntityRepository
{
    public function __construct(RegistryInterface $registry)
    {
        parent::__construct($registry, Post::class);
    }




    public function save(Post $post)
    {
        $this->getEntityManager()->persist($post);
        $this->getEntityManager()->flush();
        return $post->getId();


    }

    public function findByUserId(int $userId)
    {
        $query = $this->createQueryBuilder('u')
            ->where('u.id = :userId')
            ->setParameter('id', $userId)
            ->getQuery();
        return $query->getOneOrNullResult();
    }


}

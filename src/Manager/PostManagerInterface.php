<?php
namespace App\Manager;

use App\Entity\User\User;
use App\Entity\Post\Post;

interface PostManagerInterface
{
    public function createPost(Post $post, User $user, $userId): void ;
    public function getUserById(int $id);

   // public function getAll(): array;
   // public function getPostById(int $postId): Post;
  //  public function increaseViews($postId);
}
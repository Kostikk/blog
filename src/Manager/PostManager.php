<?php

namespace App\Manager;

use App\Entity\Post\Post;
use App\Entity\User\User;
use Doctrine\ORM\EntityManagerInterface;
use App\Manager\PostManagerInterface;




class PostManager implements PostManagerInterface
{


    private $PostRepository;
    private $UserRepository;


    public function __construct(EntityManagerInterface $entityManager)

    {
        $this->PostRepository = $entityManager->getRepository('App:Post\Post');
        $this->UserRepository = $entityManager->getRepository('App:User\User');
        //$this->PostServiceInterface=$postService;


    }



    public function createPost(Post $post, User $user, $userId): void
    {
      /*  $postExists = $this->postRepository->findOneBy(array('title' => $post->getTitle()));
        if ($postExists) {
            throw new \Exception("Post already exists");
        }*/


        $userId = $this->getUserById($userId);


        $user->addPost($post);
        $post->setUser($user);

        $post = $this->PostRepository->save($post);

       //return $userId;
    }

    public function getUserById(int $id)
    {
        $id = $this->PostRepository->find($id);
        return $id;



    }

}



